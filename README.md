### 友情提醒
MyClouds已于2019年5月停止维护。 [开发文档](https://gitee.com/pulanos/myclouds/tree/master/myclouds-docs)

### 截屏留念
![输入图片说明](https://images.gitee.com/uploads/images/2020/0418/151811_64047579_431745.jpeg "MyClouds企业级微服务开源平台-资源模块.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0418/151853_b43471fb_431745.jpeg "MyClouds企业级微服务开源平台v2.1.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0418/151904_c10c726c_431745.png "MyClouds开发框架-用户管理.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0418/151919_6d52d04c_431745.png "MyClouds开发框架-角色管理.png")

### :heartbeat: 彩蛋
##### 阿里开源技术交流微信群（ _微信扫码加入_ ）
![输入图片说明](https://images.gitee.com/uploads/images/2021/0619/020708_d3a7e67e_431745.jpeg "wechat2.jpeg")  
 :underage: 希望在这里和大家共建一个持续分享阿里相关开源技术和最佳参考实践的社群。「重点关注项目：Dubbo、Nacos、Seata、Sentinel」【分享经验-传递价值-成就你我】:100: 